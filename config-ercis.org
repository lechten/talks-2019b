# -*- eval: (visual-line-mode) -*-

# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

#+INCLUDE: "config.org"

# ERCIS slide master is not centered.
#+OPTIONS: reveal_center:nil

# ERCIS adjustments for current theme.
#+REVEAL_EXTRA_CSS: ./reveal.js/css/theme/ercis.css

# ERCIS footer with title and author to left, date to right.
# This is a hack, which is not suitable for self-study material
# (overlap upon resize, incompatible with coursemod plugin):
#+REVEAL_POSTAMBLE: <div class="reveal ercisfooter"><p class="floatleft width33">%t<br />%a</p><p class="floatright width15"><br />%d</p></div>

# Do not apply a color to the TOC progress footer as presentations here make
# use of the ERCIS theme with a colored footer.
#+OER-REVEAL-TOC-PROGRESS-DEPENDENCY: { src: '%splugin/toc-progress/toc-progress.js', async: true, callback: function() { toc_progress.initialize('reduce', 'none'); toc_progress.create(); } }
