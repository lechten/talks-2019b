# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: "config-ercis.org"

# For local generation:
# (setq oer-reveal-export-dir "./" table-html-table-attribute "class=\"emacs-table fragment\"")

#+OPTIONS: reveal_width:1200 reveal_height:900

# Use ERCIS title slide.
#+INCLUDE: "~/.emacs.d/oer-reveal-org/ercis-talk-online-qr.org"
#+REVEAL_TALK_QR_CODE: ./img/delfi2019-short.png
#+REVEAL_TALK_URL: https://lechten.gitlab.io/talks/

# Ask questions while the title slide is displayed.
#+REVEAL_TITLE_SLIDE_TIMING: 90

#+TITLE: Simplifying license attribution for OER with emacs-reveal
#+DATE: 2019-09-19, DELFI 2019
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.
#+KEYWORDS: open educational resources, OER, emacs-reveal, license, creative commons, attribution, free software, reveal.js, presentation
#+DESCRIPTION: Talk about license attribution for OER with emacs-reveal.  Given at DELFI 2019.

# The following SUBTITLE adds visible usage notes as well as invisible
# speaker notes to the title slide in an aside element.
# HTML for that element can be created by including the following
# (commented) notes block.
# TODO Figure out how to embed generated HTML (babel?).
# #+begin_notes
# 1. Language?
# 2. My approach towards OER presentations with software emacs-reveal
#    - Yet another tool, yes.  Guess why.
# 3. QR code, multiplex
# 4. Who can explain OER?
# 5. Who publishes OER?
#    - DELFI, with room for improvement.
# #+end_notes
#+SUBTITLE: @@html:<div class="usage"><p>(Press <code>?</code> for help, <code>n</code> and <code>p</code> for next and previous slide)</p></div><aside class="notes"><ol><li>Language?</li><li>My approach towards OER presentations with software emacs-reveal<ul><li>Yet another tool, yes.  Guess why.</li></ul></li><li>QR code, multiplex</li><li>Who can explain OER?</li><li>Who publishes OER?<ul><li>DELFI, with room for improvement.</li></ul></li></ol></aside>@@


* Motivation
** Reality check
   :PROPERTIES:
   :reveal_extra_attr: data-timing="120"
   :END:
   #+ATTR_REVEAL: :frag (appear)
   - Suppose you give a talk about OER
   - You find the “OER Global Logo”
     - You decide to include it
       {{{reveallicense("./figures/logos/Global_OER_Logo.svg.meta","40vh")}}}
     - You identify [[https://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution 3.0 Unported]]
       (CC BY 3.0) as applicable license terms
   - How to *locate* and *include* attribution information demanded by
     license?
#+begin_notes
Give OER talk, find logo

Logo alludes to key aspects of OER:
- Open book cover for lOERning; bOERrd for freedom
- 3 sheets of papOER: book
- Hands for education, increasing size for growth

License terms spell out requirements for licensees.  Who wants to read them?

Rules of thumb provide shorter instructions.
#+end_notes

** Incomplete rules of thumb
   :PROPERTIES:
   :CUSTOM_ID: tasl
   :reveal_extra_attr: data-timing="60"
   :END:
   - [[https://wiki.creativecommons.org/index.php?title=Best_practices_for_attribution][TASL]] by Creative Commons
     {{{reveallicense("./figures/thenounproject/noun_licensing_936051.meta","30vh")}}}
     - Title, author, source, license
   - [[https://open-educational-resources.de/oer-tullu-regel/][TULLU]] in Germany
     - Titel, Urheber*in, Lizenz, Link, Ursprungsort
   #+ATTR_REVEAL: :frag appear
   - Licensees *must* also indicate modifications, reproduce copyright
     notices and disclaimers

** Major challenge
   :PROPERTIES:
   :reveal_extra_attr: data-timing="135"
   :END:
   - *Incomplete* and *inconsistent* source information
     {{{reveallicense("./figures/thenounproject/noun_Puzzle_38442.meta","40vh")}}}
     #+ATTR_REVEAL: :frag (appear)
     - E.g., OER Global logo above
       -  *No title* but author information at [[https://commons.wikimedia.org/wiki/File:Global_Open_Educational_Resources_Logo.svg][Wikimedia Commons]]
         - [[https://commons.wikimedia.org/wiki/User:Jonathasmello][Jonathasmello]]
       #+ATTR_REVEAL: :frag (appear)
       - Conflicting title and author information in [[https://en.unesco.org/sites/default/files/global_oer_logo_manual_en.pdf][logo manual]]
         - First page, *Global OER Logo*
           #+begin_src
           2012, Jonathas Mello
           www.jonathasmello.com
           #+end_src
         - Last page, *OER Global Logo*
           #+begin_src
           Jonathas Mello
           #+end_src
   #+ATTR_REVEAL: :frag (appear)
   - Apparently, manual copying of license information by licensees
     not *feasible*
     #+ATTR_REVEAL: :frag (appear)
     - Textual specification by licensors neither
   - Identifying provenance and attribution is among the most
     time-consuming factors for OER projects cite:FLGB16
#+begin_notes
If Jonathasmello is Jonathas Mello, a title is not necessary …
#+end_notes

** OER presentations with emacs-reveal
   :PROPERTIES:
   :reveal_extra_attr: data-timing="60"
   :END:
   - [[*Emacs-reveal][Emacs-reveal]] is software to generate HTML presentations
     {{{reveallicense("./figures/thenounproject/noun_lecture_2797185.meta","30vh")}}}
     #+ATTR_REVEAL: :frag (appear)
     - With proper license attribution for images
            {{{reveallicense("./figures/thenounproject/noun_licensing_936051.meta","20vh")}}}
       - Human-readable, e.g., images on this slide
       - Machine-readable with RDFa (in HTML source code)
         - ([[#sample-rdfa][Example follows]])
     - Based on
       - (Editor) [[https://www.gnu.org/software/emacs/][GNU Emacs]]
         with [[https://orgmode.org/][Org mode]]
       - JavaScript presentation framework
         [[https://revealjs.com/][reveal.js]]

#+TOC: headlines 1

* Background
** OER - What?
   :PROPERTIES:
   :reveal_extra_attr: data-timing="60"
   :END:
   - *Open Educational Resources* (OER)
     #+ATTR_REVEAL: :frag (appear)
     - OER are
       “[[https://en.unesco.org/oer/paris-declaration][teaching, learning and research materials in any medium, digital or otherwise, that reside in the public domain or have been released under an open license that permits no-cost access, use, adaptation and redistribution by others with no or limited restrictions.]]”
       {{{reveallicense("./figures/logos/Global_OER_Logo.svg.meta","30vh")}}}
       #+ATTR_REVEAL: :frag appear
       - Usually, Creative Commons licensing
         - Permit 5 Rs of openness cite:Wil14,HWSJ10:
           /retain/, /reuse/, /revise/, /remix/, /redistribute/
     - In addition to license requirements, also technical requirements
       - E.g., ALMS framework cite:HWSJ10, extended in cite:Lec19
         - ([[#alms][Backup slides]])
         - ([[#org-markup][Org source for this slide]])
   #+begin_notes
   - UNESCO as proponent for OER
   - Licensing is crucial
   - [[https://open-educational-resources.de/5rs-auf-deutsch/][5 Rs as Vs in German]]:
     verwahren/vervielfältigen, verwenden, verarbeiten, vermischen, verbreiten
   #+end_notes

** OER - Why?
   :PROPERTIES:
   :reveal_extra_attr: data-timing="140"
   :END:
   - Global perspective: [[https://sustainabledevelopment.un.org/sdg4][SDG4]],
     world peace
     #+ATTR_REVEAL: :frag appear
     - “Open Educational Resources (OER) support quality education that
       is equitable, inclusive, open and participatory.” cite:Une17
       {{{revealgrid(1,"./figures/thenounproject/oer-global.grid",20,4,1,"\"ga1 ga2 ga3 ga4\"",t)}}}
     #+ATTR_REVEAL: :frag appear
     - Other reasons
       {{{revealgrid(2,"./figures/thenounproject/oer-local.grid",12,5,1,"\"ga1 ga2 ga3 ga4 ga5\"",t)}}}
     #+ATTR_REVEAL: :frag appear
     - General movement for freedom and transparency
       {{{reveallicense("./figures/thenounproject/noun_Transparency_1864165.meta","15vh")}}}
       #+ATTR_REVEAL: :frag appear
       - Open Access, Open Data, *Open Education*, Open Science,
         *(Free/Libre and) Open Source Software*
#+begin_notes
- 17 SDGs set in 2015 for 2030
- Education is sharing of knowledge and values
- Re-use of experts’ resources simplifies creation of high-quality teaching resources
- Re-use avoids redundant work when thousands of teachers/professors
  create their own resources for the same topic in different places
- OER enable Open Educational Practices
  - Interactions around OER
  - Empowerment of students (create/improve resources, teach others)
- Visibility and Reputation
  - Publication of OER demonstrates expertise, may attract
    students, stimulate research cooperation
  - Why hide your resources in LMS?
- Related mind sets
  - Wiki culture
  - Make spaces, four Rs (reuse, reduce, recycle,
    repair); requires FLOS software/hardware/instructions
#+end_notes

** Emacs-reveal
   :PROPERTIES:
   :reveal_extra_attr: data-timing="120"
   :END:
   - [[https://en.wikipedia.org/wiki/Free_and_open-source_software][Free/libre and open source software]]
     (FLOSS) to create OER presentations cite:Lec19b
     #+ATTR_REVEAL: :frag (appear)
     - (FLOSS: [[https://www.gnu.org/philosophy/free-sw.html.en][4 freedoms]],
       similar to 5 Rs above, but going back to 1980s cite:Sta86)
     - HTML slideshows with audio explanations
       {{{reveallicense("./figures/thenounproject/noun_Online Resources_2797174.meta","30vh")}}}
       - To be viewed with standard Web browsers (platform independent),
         either on- or offline
       - Features include
         - Animations and slide transitions; speaker’s view with
           preview, notes, and timer; embedding of images, audio,
           video, mathematical formulas; table of contents;
           bibliography; keyword index; hyperlinks within and between
           presentations; themes for different styling; responsive
           design with touch support; quizzes for retrieval practice;
           code highlighting and evaluation for programming languages
     - Contents separated from layout with lightweight markup language
       [[https://orgmode.org/][Org mode]]
       - Enables single sourcing cite:Roc01
       - Enables collaboration with version control, e.g., [[https://git-scm.com][Git]]
         {{{reveallicense("./figures/logos/Git-Logo-2Color.meta","8vh")}}}
   #+begin_notes
   FLOSS: Development community.

   One goal: Take back control over devices/services that do not work
   in our interests
   #+end_notes

* Attribution with emacs-reveal
** CC attribution requirements
   :PROPERTIES:
   :CUSTOM_ID: license-metadata
   :reveal_extra_attr: data-timing="60"
   :END:

   - [[#ccrel][CC REL]] is a standard for machine-readable licensing information cite:AAL+12
     - As [[#tasl][TASL and TULLU]], CC REL is incomplete
   - Emacs-reveal extends CC REL in pragmatic ways

#+ATTR_HTML: :frag appear
+-----------------------------+---------------------+-------------------------+
| Requirement                 | CC REL              | emacs-reveal            |
+-----------------------------+---------------------+-------------------------+
| Name creator(s)             | cc:attributionName, | cc:attributionName,     |
|                             | cc:attributionURL   | cc:attributionURL       |
+-----------------------------+---------------------+-------------------------+
| Reproduce copyright notice  | -                   | copyright               |
+-----------------------------+---------------------+-------------------------+
| Reproduce license notice    | license             | licenseurl, licensetext |
+-----------------------------+---------------------+-------------------------+
| Reproduce disclaimer of     | -                   | copyright, permit       |
| warranties                  |                     |                         |
+-----------------------------+---------------------+-------------------------+
| Include hyperlink to OER    | dc:source           | dc:source, sourcetext   |
+-----------------------------+---------------------+-------------------------+
| Indicate modifications      | -                   | imgadapted              |
+-----------------------------+---------------------+-------------------------+
| Indicate license            | license             | licenseurl, licensetext |
+-----------------------------+---------------------+-------------------------+
| -                           | dc:title            | dc:title, imgalt        |
+-----------------------------+---------------------+-------------------------+
| -                           | cc:morePermissions  | permit                  |
+-----------------------------+---------------------+-------------------------+

   #+begin_notes
   - TASL: Title, author, source, license
   - TULLU: Titel, Urheber*in, Lizenz, Link, Ursprungsort
   #+end_notes

** Metadata for OER logo
   :PROPERTIES:
   :reveal_extra_attr: data-timing="40"
   :END:
   - Sample metadata with license information
     - Stored in text file [[https://gitlab.com/oer/figures/blob/master/logos/Global_OER_Logo.svg.meta][Global_OER_Logo.svg.meta]]
       - As part of my [[https://gitlab.com/oer/figures/][OER figure collection]]

#+INCLUDE: "./figures/logos/Global_OER_Logo.svg.meta" src emacs-lisp -n 1

#+begin_notes
- Whenever I copy a figure, I create such a metadata file
- Feel free to contribute
#+end_notes

** Sample markup
   :PROPERTIES:
   :CUSTOM_ID: org-markup
   :reveal_extra_attr: data-timing="90"
   :END:
   - [[https://orgmode.org][Org mode]] source code for
     [[*OER - What?][earlier slide]]
     #+begin_src org
,** OER - What?
   - *Open Educational Resources* (OER)
     #+ATTR_REVEAL: :frag (appear)
     - OER are
       “[[https://en.unesco.org/oer/paris-declaration][teaching, learning and research materials in any medium, digital or otherwise, that reside in the public domain or have been released under an open license that permits no-cost access, use, adaptation and redistribution by others with no or limited restrictions.]]”
       {{{reveallicense("./figures/logos/Global_OER_Logo.svg.meta","30vh")}}}
       #+ATTR_REVEAL: :frag appear
       - Usually, Creative Commons licensing
         - Permit 5 Rs of openness cite:Wil14,HWSJ10:
           /retain/, /reuse/, /redistribute/, /revise/, /remix/
     - In addition to license requirements, also technical requirements
       - E.g., ALMS framework cite:HWSJ10, extended in cite:Lec19
         - ([[#alms][Backup slides]])
         - ([[#org-markup][Org source for this slide]])
     #+end_src
     - Note
       - Simple markup, e.g.:
         - ~**~ for 2nd-level heading; ~-~ with indentation for nested lists;
           ~*~ and ~/~ for bold and italics
         - Citations to BibTeX bibliography with ~cite~, animations
           with ~:frag appear~
       - OER logo embedded with ~reveallicense~
         - [[*Metadata for OER logo][Metadata of previous slide]]
           handled automatically

** Sample RDFa for OER logo (simplified)
   :PROPERTIES:
   :CUSTOM_ID: sample-rdfa
   :reveal_extra_attr: data-timing="60"
   :END:
#+BEGIN_SRC HTML -n 1
<div about="Global_OER_Logo.svg.png" class="figure">
  <img src="Global_OER_Logo.svg.png" alt="OER Global Logo" />
  <span property="dc:title">OER Global Logo</span>
  by <a rel="cc:attributionURL dc:creator" href="https://commons.wikimedia.org/wiki/User:Jonathasmello" property="cc:attributionName">Jonathasmello</a>
  under <a rel="license" href="https://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported</a>;
  from <a rel="dc:source" href="https://commons.wikimedia.org/wiki/File:Global_Open_Educational_Resources_Logo.svg">Wikimedia Commons</a>
</div>
#+END_SRC

  {{{revealimg("./figures/logos/Global_OER_Logo.svg.meta","","30vh")}}}

  (Hint: Inspect RDFa with browser extension
  [[http://osds.openlinksw.com/][OpenLink Structured Data Sniffer]])

* Conclusions
** Spread the wOERd
   :PROPERTIES:
   :reveal_extra_attr: data-timing="40"
   :END:
   - OER “support quality education that is equitable, inclusive, open
     and participatory.” cite:Une17
     #+ATTR_REVEAL: :frag appear
     - CC licensing can provide freedom
     - Emacs-reveal is a tool to create OER presentations and helps
       with licensing attribution

   {{{revealgrid(1,"./figures/thenounproject/oer-global.grid",20,4,1,"\"ga1 ga2 ga3 ga4\"",grid)}}}

   {{{revealgrid(2,"./figures/thenounproject/oer-local.grid",12,5,1,"\"ga1 ga2 ga3 ga4 ga5\"",grid)}}}

** A jOERney ahead
   :PROPERTIES:
   :reveal_extra_attr: data-timing="30"
   :END:
   - Emacs-reveal adds [[#license-metadata][necessary metadata]] in pragmatic ways
     {{{reveallicense("./figures/3d-man/steps.meta","40vh",,t)}}}
     - Further standardization?
     - Aesthetics vs legal requirements?
     - Your suggestions?

** It’s your tOERn
   :PROPERTIES:
   :CUSTOM_ID: questions-ercis-slide
   :reveal_background: #852339
   :reveal_extra_attr: data-timing="10"
   :reveal_data_state: no-toc-progress
   :END:

   #+HTML: <p><br /></p>
   {{{revealimg("./figures/3d-man/question-mark-2314109.meta","","60vh",,t)}}}

#+HTML: <div><p><br /></p><p><br /></p><p class="floatleft ercisisnetwork">THE <span style="color:rgb(195,148,146);">IS</span> RESEARCH NETWORK</p><p class="floatright"><a class="nooutlink ercisurl" href="https://www.ercis.org/">www.ercis.org</a></p></div>

#+begin_notes
If time permits
- Mention [[#oer-infrastructure][OER infrastructure]]
- Show @@html:<a href="#slide-bibliography">bibliography</a> and <a href="#slide-license">license slide</a>@@
- Show speaker’s view
- Show emacs
#+end_notes

* Backup
  :PROPERTIES:
  :UNNUMBERED: notoc
  :HTML_HEADLINE_CLASS: no-toc-progress
  :reveal_extra_attr: data-timing="0"
  :END:
** License metadata
   :PROPERTIES:
   :CUSTOM_ID: ccrel
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :reveal_extra_attr: data-timing="0"
   :END:
   - License information should be machine-readable
     {{{reveallicense("./figures/thenounproject/noun_licensing_936051.meta","30vh")}}}
     - Overcome above challenges
     - Improve tool support in general
   - Creative Commons Rights Expression Language (CC REL) cite:AAL+12
     - RDF vocabulary to express (some) licensing aspects
       {{{reveallicense("./figures/thenounproject/noun_jigsaw puzzle_913729.meta","10vh")}}}
       - (See [[#license-metadata][revisited]])
   - RDF: Resource description framework, semantic web approach cite:Hor08
     - Specify knowledge in triples: [[color:darkgreen][subject]], [[color:darkred][predicate]], [[color:darkblue][object]]
       - E.g., [[color:darkgreen][Jens]] [[color:darkred][is author of]] [[color:darkblue][a specific document]]
     - RDFa: Embedding of RDF into HTML

** ALMS framework
   :PROPERTIES:
   :CUSTOM_ID: alms
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :reveal_extra_attr: data-timing="0"
   :END:

   Criteria for OER software proposed in cite:HWSJ10

+---------------------+--------------------+----------------------------+
| ALMS criterion      | Examples           | Counter examples           |
+---------------------+--------------------+----------------------------+
| <b>Access</b> to    | Free/Libre and     | Powerpoint                 |
| editing tools       | Open Source Soft-  | Google Docs                |
|                     | ware (e.g., LaTeX, |                            |
|                     | LibreOffice)       |                            |
+---------------------+--------------------+----------------------------+
| <b>Level</b> of     |                                                 |
| expertise required  |               Challenging topic …               |
| to revise or remix  |                                                 |
+---------------------+--------------------+----------------------------+
| <b>Meaningfully</b> | LaTeX, Org Mode    | (Scanned) PDF,             |
| editable            | (HTML)             | flash, video               |
+---------------------+--------------------+----------------------------+
| <b>Source-file</b>  | LaTeX, Org Mode    | PDF for LaTeX              |
| access              | (HTML)             | PDF for office presentation|
+---------------------+--------------------+----------------------------+

** More requirements
   :PROPERTIES:
   :CUSTOM_ID: oer-requirements
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :reveal_extra_attr: data-timing="0"
   :END:
   - Extension of ALMS framework cite:Lec19
     - Requirements for “A”: [[https://en.wikipedia.org/wiki/Free_and_open-source_software][Free/libre and open source software (FLOSS)]]
       - FLOSS for learners, teachers, OER users and creators
       - *Platform independent*
       - Also *mobile* and *offline*
     - Requirements for “M” and “S”: *Single sourcing* cite:Roc01
       - *Single*, collaboratively maintained source, *no* Copy&Paste
       - *Separation* of contents and layout
       - Source files with *lightweight markup* for collaboration with
         comparison and integration of *versions*
   - [[*Emacs-reveal][Emacs-reveal]] satisfies above requirements

** OER infrastructure on GitLab
   :PROPERTIES:
   :CUSTOM_ID: oer-infrastructure
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :reveal_extra_attr: data-timing="0"
   :END:
   - [[https://about.gitlab.com/community/][GitLab]] is a FLOSS DevOps platform
     {{{reveallicense("./figures/logos/GitLab-wm_no_bg.meta","12vh")}}}
     - Hosting [[https://gitlab.com/oer/emacs-reveal][emacs-reveal]]
       and [[https://oer.gitlab.io/][my OER]] as GitLab Pages
     - You could run your own instance if you preferred
   - OER are re-generated upon source changes
     - GitLab CI/CD pipelines with
       [[https://gitlab.com/oer/docker][Docker images]]
       {{{reveallicense("./figures/logos/docker-horizontal.meta","12vh")}}}

#+MACRO: copyrightyears 2019
#+MACRO: licensepreamble
#+INCLUDE: "~/.emacs.d/oer-reveal-org/backmatter.org"

# Local Variables:
# indent-tabs-mode: nil
# End:
