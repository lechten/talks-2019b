# -*- eval: (visual-line-mode) -*-

# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

#+LANGUAGE: en
#+INCLUDE: "~/.emacs.d/oer-reveal-org/config.org"

# Only export sub- and superscripts when used with braces.
# This avoids accidental interpretations, e.g., with underscores in
# names of files or variables.
#+OPTIONS: ^:{}

# Set a default timing of 20 seconds per slide.  This is deliberately
# low to work with generated slides (title, TOC, section headings).
# On "real" slides, their timing needs to be specified with
# data-timing attributes then.
#+OPTIONS: reveal_defaulttiming:20

# GitLab ribbon with legalese:
#+REVEAL_PREAMBLE: <div class="ribbon-legalese"><p><a href="https://gitlab.com/lechten/talks">Fork me on GitLab</a></p><p><a href="/imprint.html">Imprint</a> | <a href="/privacy.html">Privacy Policy</a></p></div>
