;;; publish.el --- Publish reveal.js presentations from Org files
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2019 Jens Lechtenbörger

;;; License: GPLv3

;;; Commentary:
;; Publication of Org source files to reveal.js uses Org export
;; functionality offered by org-re-reveal and oer-reveal.
;; Initialization code for both is provided by emacs-reveal.
;; Note that org-re-reveal and oer-reveal are available on MELPA.
;;
;; Use this file from its parent directory with the following shell
;; command:
;; emacs --batch --load elisp/publish.el

;;; Code:
(package-initialize)

;; Try most recent libraries if available.
(add-to-list 'load-path (expand-file-name "../../../oer/org-re-reveal" (file-name-directory load-file-name)))
(add-to-list 'load-path (expand-file-name "../../../oer/oer-reveal" (file-name-directory load-file-name)))
(require 'org-re-reveal)
(require 'oer-reveal)
(require 'oer-reveal-publish)

;; Load emacs-reveal.
(add-to-list 'load-path (expand-file-name "~/.emacs.d/elpa/emacs-reveal"))
(require 'emacs-reveal)

;; Generate multiplex client presentations if name contains "multiplex".
(setq org-re-reveal-client-multiplex-filter "multiplex")
(add-to-list 'oer-reveal-publish-org-publishing-functions
             #'oer-reveal-publish-to-reveal-client)

;; Activate Emacs Lisp and Org mode for babel.
(org-babel-do-load-languages
 'org-babel-load-languages '((emacs-lisp . t) (org . t)))

;; Use local klipse-libs for offline use.
(add-to-list 'oer-reveal-plugins "klipse-libs")
(setq org-re-reveal-klipse-extra-css "<style>
/* Position computations of klipse get confused by reveal.js's scaling.
   Hence, scaling should be disabled with this code.  Fix height of code area
   with scrollbar (use overflow instead of overflow-y to restore CodeMirror
   setting afterwards): */
.reveal section pre { max-height: 60vh; height: auto; overflow: auto; }
/* Reset some reveal.js and oer-reveal settings: */
.reveal section pre .CodeMirror pre { font-size: 1em; box-shadow: none; width: auto; padding: 0.1em; display: block; overflow: visible; }
/* Enlarge cursor: */
.CodeMirror-cursor { border-left: 3px solid black; }
</style>\n")

;; Publish Org files.
(oer-reveal-publish-all
 (list
  (list "images"
	:base-directory "img"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/img"
	:publishing-function 'org-publish-attachment)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)))
;;; publish.el ends here
